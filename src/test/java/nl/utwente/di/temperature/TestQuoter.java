package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        TempConverter quoter = new TempConverter();
        double price = quoter.toFahrenheit("0");
        Assertions.assertEquals(32.0, price, 0.0, "Celsius 0 to Fahrenheit");
    }
}
