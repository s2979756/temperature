package nl.utwente.di.temperature;

public class TempConverter {
    public double toFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 9/5) + 32;
    }
}